﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace APITest2 {
    /// <summary>
    /// This script creates an HttpWebRequest object and passes it a SOAP action in the form of an XmlDocument object, 
    /// then resolves that request to an HttpWebResponse object from which it streams the results into an XML file that
    /// is saved on the user's disk.
    /// The soap action it performs is currently hardcoded.
    /// 
    /// Written by Lachlan Watson for QSuper Group on behalf of Upstream Bears, 2019
    /// </summary>
    class Program {
        /// <summary>
        /// setupRequest takes the soap action specified and attempts to create an HttpWebRequest object that uses the relevant headers
        /// </summary>
        /// <param name="_action">the soap action to be performed</param>
        /// <returns>an HttpWebRequest object</returns>
        static HttpWebRequest setupRequest(string _action) {
            string action = _action;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://srveagdv19/ArcherConnector/V1");
            request.Headers.Add("APIKey", "fe7f8ace83194e4099f3397bcd6f1665");
            request.Headers.Add("AcceptEncoding", "gzip,deflate");
            request.ContentType = "application/soap+xml;charset=UTF-8;action=" + '"' + action + '"';
            request.Host = "srveagdv19";
            request.Method = "POST";
            request.KeepAlive = true;
            return request;
        }

        /// <summary>
        /// Combines the HttpWebRequest object with the soap envelope so that the soap action can be successfully performed
        /// </summary>
        /// <param name="_request">the HttpWebRequest that will have the soap XML inserted into it</param>
        private static void combineRequestAndSoap(HttpWebRequest _request) {
            using (Stream stream = _request.GetRequestStream()) {
                XmlDocument soapEnvelope = loadSoap();
                soapEnvelope.Save(stream);
            }
        }

        /// <summary>
        /// Creates an XmlDocument object that contains the necessary soap information for the action to be performed successfully
        /// </summary>
        /// <returns>An XmlDocument object that will execute the required action when passed to the URL</returns>
        private static XmlDocument loadSoap() {
            XmlDocument soapEnvelope = new XmlDocument();
            soapEnvelope.LoadXml(@"<soap:Envelope xmlns:soap=""http://www.w3.org/2003/05/soap-envelope"" xmlns:v3=""http://www.qsuper.qld.gov.au/schema/ICC/Header/v3.0"" xmlns:v1=""http://www.qsuper.com.au/services/ArcherConnector/GetMaterialRisks/V1"">"
                            + "<soap:Body><v1:getMaterialRisksRequest></v1:getMaterialRisksRequest></soap:Body></soap:Envelope>");
            return soapEnvelope;
        }

        /// <summary>
        /// Saves a copy of the soap response to an XML file in the same folder as the project lives.
        /// Formats the file name as the soap action performed plus the current date.
        /// </summary>
        /// <param name="_request">The HttpWebRequest object</param>
        /// <param name="_action">The soap action that was performed</param>
        static void dumpResponseToDisk(HttpWebRequest _request, string _action) {
            HttpWebRequest request = _request;
            string action = _action;
            try {
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse()) {
                    using (StreamReader reader = new StreamReader(response.GetResponseStream())) {
                        //reads the response from the HttpWebRequest object and writes it to an XML file
                        string responseText = reader.ReadToEnd();
                        byte[] responseBytes = Encoding.Default.GetBytes(responseText);
                        string fileName = action + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xml";
                        using (var fs = new FileStream(fileName, FileMode.Create, FileAccess.Write)) {
                            fs.Write(responseBytes, 0, responseBytes.Length);
                        }
                        Console.WriteLine("Wrote " + fileName + " to disk successfully");
                        reader.Close();
                    }
                    response.Close();
                }
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
                Console.WriteLine("We hit an exception, hit any key to close.");
                Console.ReadKey();
            }
        }

        /// <summary>
        /// The main entry point for the program. Currently hardcoded to perform the soap action 'getMaterialRisksV1' but could
        /// be edited to take the soap action as an argument when the script is called.
        /// </summary>
        /// <param name="args">currently unused</param>
        static void Main(string[] args) {
            //string action = args[0];
            string action = "getMaterialRisksV1";
            Console.WriteLine("setting up request");
            HttpWebRequest request = setupRequest(action);
            Console.WriteLine("combining request");
            combineRequestAndSoap(request);
            Console.WriteLine("getting response and saving to disk");
            dumpResponseToDisk(request, action);
            Console.WriteLine("file export done... waiting for user input to exit");
            Console.ReadKey();
        }

    }//end class
}//end namespace
